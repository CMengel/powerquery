let
    // I. Connection to data source
    Source =
        Snowflake.Databases(
            CURRENT_SERVER,
            CURRENT_WAREHOUSE,
            [
                Role = CURRENT_ROLE,
                CreateNavigationProperties = null,
                ConnectionTimeout = null,
                CommandTimeout = null
            ]
        ),
    CTE_PRE_PROD_DB_Database =
        Source{[
            Name = CURRENT_DATABASE,
            Kind = "Database"
        ]}
            [Data],
    CTE_DW_SCHEMA_Schema =
        CTE_PRE_PROD_DB_Database{[
            Name = CURRENT_SCHEMA_FACT,
            Kind = "Schema"
        ]}
            [Data],
    VW_CN_FACT_CUSTOMER_STATUS_TOT_INV_View =
        CTE_DW_SCHEMA_Schema{[
            Name = "VW_CN_FACT_CUSTOMER_STATUS_TOT_INV",
            Kind = "View"
        ]}
            [Data],
    // II. Specify the desired columns to import from the warehouse
    #"Removed Columns" =
        Table.RemoveColumns(
            VW_CN_FACT_CUSTOMER_STATUS_TOT_INV_View,
            {"IS_ACTIVE"}
        ),
    #"Added Conditional Column" =
        Table.AddColumn(
            #"Removed Columns",
            "SORT_ORDER",
            each
                if [RENTAL_CUSTOMER_STATUS] = "New" then
                    1
                else if [RENTAL_CUSTOMER_STATUS] = "Active" then
                    2
                else if
                    [RENTAL_CUSTOMER_STATUS]
                    = "Approaching Dormant"
                then
                    3
                else if [RENTAL_CUSTOMER_STATUS] = "Dormant" then
                    4
                else if [RENTAL_CUSTOMER_STATUS] = "Inactive" then
                    5
                else if [RENTAL_CUSTOMER_STATUS] = "Lost" then
                    6
                else
                    99,
            Int64.Type
        ),
    // III. Filter, Remove Rows, Replace Values
    #"Filtered Rows" =
        Table.SelectRows(
            #"Added Conditional Column",
            each true
        ),
    // IV. Specify the data type of each column to maximize accuracy & performance
    #"Changed Type" =
        Table.TransformColumnTypes(
            #"Filtered Rows",
            {
                {
                    "CUSTOMER_STATUS_PK",
                    Int64.Type
                },
                {
                    "CUSTOMER_FK",
                    Int64.Type
                },
                {
                    "EMPLOYEE_FK",
                    Int64.Type
                },
                {
                    "DIVISION_FK",
                    Int64.Type
                },
                {
                    "STATUS_DATE_FK",
                    Int64.Type
                },
                {
                    "LAST_INVOICE_DATE_FK",
                    Int64.Type
                },
                {
                    "CURR_LTD_INV_AMT",
                    Currency.Type
                },
                {
                    "CURR_YTD_INV_AMT",
                    Currency.Type
                },
                {
                    "CURR_MTD_INV_AMT",
                    Currency.Type
                }
            }
        )
in
    #"Changed Type"