let
    // I. Connection to data source
    Source =
        Snowflake.Databases(
            CURRENT_SERVER,
            CURRENT_WAREHOUSE,
            [
                Role = CURRENT_ROLE,
                CreateNavigationProperties = null,
                ConnectionTimeout = null,
                CommandTimeout = null
            ]
        ),
    CTE_DEV_DB_Database =
        Source{[
            Name = CURRENT_DATABASE,
            Kind = "Database"
        ]}
            [Data],
    CTE_DW_SCHEMA_Schema =
        CTE_DEV_DB_Database{[
            Name = CURRENT_SCHEMA,
            Kind = "Schema"
        ]}
            [Data],
    // II. Specify the desired columns to import from the warehouse
    DIM_GL_ACCOUNT_Table =
        Table.SelectColumns(
            CTE_DW_SCHEMA_Schema{[
                Name = "DIM_GL_ACCOUNT",
                Kind = "Table"
            ]}
                [Data],
            {
                "GL_ACCOUNT_PK",
                "GL_ACCOUNT_NUM",
                "GL_ACCOUNT_NAME",
                "GL_ACCOUNT_TYPE",
                "IS_ACTIVE",
                "CRTD_DTTM",
                "UPDT_DTTM",
                "SRC_TYP",
                "MD5",
                "CRTD_BY",
                "UPDT_BY",
                "SRC_SUB_TYP",
                "GL_ACCOUNT_CLASS",
                "ACCT_GRP_NUM",
                //  further breakdown of GL_ACCOUNT_CLASS
                "ACCT_GRP_DESC"
            }
        ),
    #"Filtered rows" =
        Table.SelectRows(
            DIM_GL_ACCOUNT_Table,
            each [SRC_TYP] = "S4HANA"
        ),
    #"Changed column type" =
        Table.TransformColumnTypes(
            #"Filtered rows",
            {
                {
                    "GL_ACCOUNT_PK",
                    Int64.Type
                },
                {
                    "GL_ACCOUNT_NUM",
                    Int64.Type
                }
            }
        )
in
    #"Changed column type"