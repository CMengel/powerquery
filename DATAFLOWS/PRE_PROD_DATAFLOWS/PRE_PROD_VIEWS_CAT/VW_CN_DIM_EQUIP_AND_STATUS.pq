let
    // I. Connection to data source
    Source =
        Snowflake.Databases(
            CURRENT_SERVER,
            CURRENT_WAREHOUSE,
            [
                Role = CURRENT_ROLE,
                CreateNavigationProperties = null,
                ConnectionTimeout = null,
                CommandTimeout = null
            ]
        ),
    CTE_PRE_PROD_DB_Database =
        Source{[
            Name = CURRENT_DATABASE,
            Kind = "Database"
        ]}
            [Data],
    CTE_DW_SCHEMA_Schema =
        CTE_PRE_PROD_DB_Database{[
            Name = CURRENT_SCHEMA_FACT,
            Kind = "Schema"
        ]}
            [Data],
    VW_CN_DIM_EQUIP_AND_STATUS_View =
        CTE_DW_SCHEMA_Schema{[
            Name = "VW_CN_DIM_EQUIP_AND_STATUS",
            Kind = "View"
        ]}
            [Data],
    // II. Specify the desired columns to import from the warehouse
    #"Removed Other Columns" =
        Table.SelectColumns(
            VW_CN_DIM_EQUIP_AND_STATUS_View,
            {
                "EQUIP_EQUIPMENT_PK",
                "SUB_DIVISION_CD",
                "EQUIP_EQUIPMENT_ASKING",
                "EQUIP_ACQUISITION_DATE",
                "EQUIP_EQUIPMENT_CATEGORY_GROUP_DESCRIPTION",
                "EQUIP_EQUIPMENT_CATEGORY_GROUP_NUM",
                "EQUIP_EQUIPMENT_ID",
                "EQUIP_FULL_EQUIPMENT_MODEL",
                "EQUIP_EQUIPMENT_CATEGORY_PRODUCT_LINE",
                "EQUIP_SERIAL_NUM",
                "EQUIP_TOTAL_ACQUISITION_COST",
                "EQUIP_EQUIPMENT_FMV",
                "EQUIP_ANALYSIS_GROUP",
                "EQUIP_EQUIPMENT_MODEL",
                "EQUIP_EQUIPMENT_FAMILY",
                "EQUIP_EQUIPMENT_TYPE",
                "EQUIP_EQUIPMENT_CLASS",
                "EQUIP_ISRERENT"
            }
        ),
    // III. Filter, Remove Rows, Replace Values, Custom Columns - no filters needed
    // IV. Selct the data types for each column according to best practices
    // Maximize performance, efficiency and accuracy of the data model
    #"Changed Type" =
        Table.TransformColumnTypes(
            #"Removed Other Columns",
            {
                {
                    "EQUIP_EQUIPMENT_PK",
                    Int64.Type
                },
                {
                    "EQUIP_EQUIPMENT_ASKING",
                    Currency.Type
                },
                {
                    "EQUIP_TOTAL_ACQUISITION_COST",
                    Currency.Type
                },
                {
                    "EQUIP_EQUIPMENT_FMV",
                    Currency.Type
                }
            }
        )
in
    #"Changed Type"