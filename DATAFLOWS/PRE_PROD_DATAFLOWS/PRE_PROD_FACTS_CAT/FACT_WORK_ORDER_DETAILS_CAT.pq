let
    // I. Connection to data source
    Source =
        Snowflake.Databases(
            CURRENT_SOURCE,
            CURRENT_WAREHOUSE,
            [
                Role = CURRENT_ROLE,
                CreateNavigationProperties = null,
                ConnectionTimeout = null,
                CommandTimeout = null
            ]
        ),
    CTE_DB_DATABASE_Database =
        Source{[
            Name = CURRENT_DATABASE,
            Kind = "Database"
        ]}
            [Data],
    CTE_DW_SCHEMA_Schema =
        CTE_DB_DATABASE_Database{[
            Name = CURRENT_SCHEMA_FACT,
            Kind = "Schema"
        ]}
            [Data],
    // II. Specify the desired columns to import from the warehouse
    FACT_WORK_ORDER_DETAILS_CAT_Table =
        Table.SelectColumns(
            CTE_DW_SCHEMA_Schema{[
                Name = "FACT_WORK_ORDER_DETAILS_CAT",
                Kind = "Table"
            ]}
                [Data],
            {
                "WORK_ORDER_DETAIL_CAT_PK",
                "TECHNICIAN_FK",
                "GEOGRAPHY_FK",
                "REWORK_RESP_TECH_FK",
                "COST_CENTER",
                "DIVISION_FK",
                "WO_CUSTOMER_FK",
                "WO_HDR_INV_DATE_FK",
                "WO_LINE_ITEM_DATE_FK",
                "WO_LI_START_DATE_FK",
                "WO_LI_END_DATE_FK",
                "WO_HDR_STATUS",
                "WO_HDR_INV_DATE",
                "WO_LINE_TYPE",
                "WO_HEADER_ID",
                "WORK_ORDER_SUMMARY_FK",
                "LABOR_CHARGE_CODE",
                "WO_SEGMENT",
                "WORK_ORDER_SEGMENT_FK",
                "WO_LINE_ITEM_CLASS_CODE",
                "WO_LINE_ITEM_DATE",
                "WO_LINE_ITEM_NUM",
                "WO_LABOR_OT_IND",
                "WO_LABOR_OT_CUST_ALLOW_IND",
                "WO_LABOR_GRIEF_IND",
                "WO_LI_START_DATE_TS",
                "WO_LI_END_DATE_TS",
                "WO_LI_TOTAL_COST",
                "WO_LI_QUANTITY",
                "WO_LI_TOTAL_SELL",
                "WO_LI_SELL_ADJUSTMENT_RATE",
                "WO_LI_SELL_ADJUSTMENT_PROG",
                "WO_LI_COST_RATE",
                "WO_LI_PRICE_RATE",
                "WO_LI_OVERRIDE_TOTAL_PRICE",
                "WO_PART_SOURCE_OF_SUPPLY",
                "COMMODITY_CODE",
                "WO_PART_NUMBER",
                "WO_LINE_ITEM_CLASS_CODE_DESC",
                "SRC_TYP",
                "IS_ACTIVE",
                "CRTD_DTTM",
                "UPDT_DTTM",
                "MD5",
                "CRTD_BY",
                "UPDT_BY",
                "WO_LI_SELL_RATE",
                "WO_LI_FLAT_RATE_OVERRIDE_RATE",
                "WO_SEGMENT_TYPE_SPLIT",
                "WO_HDR_CLASS_CODE",
                "BACKOUT_INDICATOR",
                "WO_LI_LIST_RATE"
            }
        ),
    #"Filtered rows" =
        Table.SelectRows(
            FACT_WORK_ORDER_DETAILS_CAT_Table,
            each
                [WO_HDR_INV_DATE]
                > EARLIEST_LABOR_LAST_ADD_DATE
        )
in
    #"Filtered rows"