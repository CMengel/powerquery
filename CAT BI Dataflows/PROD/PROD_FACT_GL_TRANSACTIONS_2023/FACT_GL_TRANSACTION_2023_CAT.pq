let
    // I. Connection to data source
    Source =
        Snowflake.Databases(
            CURRENT_SOURCE,
            CURRENT_WAREHOUSE,
            [
                Role = CURRENT_ROLE,
                CreateNavigationProperties = null,
                ConnectionTimeout = null,
                CommandTimeout = null
            ]
        ),
    CTE_DB_DATABASE_Database =
        Source{[
            Name = CURRENT_DATABASE,
            Kind = "Database"
        ]}
            [Data],
    CTE_DW_SCHEMA_Schema =
        CTE_DB_DATABASE_Database{[
            Name = CURRENT_SCHEMA_FACT,
            Kind = "Schema"
        ]}
            [Data],
    // II. Specify the desired columns to import from the warehouse
    FACT_GL_TRANSACTION_CAT_Table =
        Table.SelectColumns(
            CTE_DW_SCHEMA_Schema{[
                Name = "FACT_GL_TRANSACTION_CAT",
                Kind = "Table"
            ]}
                [Data],
            {
                "GL_TRANSACTION_PK",
                "COMPANY_CODE",
                "GL_ACCOUNT_FK",
                "GEOGRAPHY_FK",
                "GL_ACCOUNT",
                "GL_ACCOUNT_SHORT_TEXT",
                "COMPANY_CODE_CURRENCY_VALUE",
                "DOCUMENT_DATE",
                "POSTING_DATE",
                "DOCUMENT_TYPE",
                "DOCUMENT_NUMBER",
                "DOCUMENT_HEADER_TEXT",
                "REFERENCE_DOC_NUM",
                "ENTRY_CODE",
                "INVOICE_NUM",
                "TRANSACTION_REFERENCE",
                "TEXT",
                "DIVISION",
                "OFFSETTING_ACCOUNT",
                "ASSIGNMENT",
                "VENDOR",
                "CUSTOMER",
                "USER_NAME",
                "CUSTOMER_ACCOUNT_NAME_1",
                "COMPANY_CODE_CURRENCY_KEY",
                "FISCAL_YEAR",
                "POSTING_DATE_FK",
                "DOCUMENT_DATE_FK",
                "PROFIT_CENTER_SHORT_TEXT",
                "COST_CENTER_SHORT_TEXT",
                "VENDOR_NAME",
                "GL_AMOUNT",
                "GL_LINE_ITEM",
                "CREDIT_DEBIT_INDICATOR",
                "POSTING_PERIOD",
                "IS_ACTIVE",
                "CRTD_DTTM",
                "UPDT_DTTM",
                "SRC_TYP",
                "MD5",
                "CRTD_BY",
                "UPDT_BY",
                "PROFIT_CENTER_FK",
                "COST_CENTER_FK",
                "PO_NUM",
                "PO_ITEM_NUM",
                "PO_ITEM_SHORT_TEXT"
            }
        ),
    #"Filtered rows" =
        Table.SelectRows(
            FACT_GL_TRANSACTION_CAT_Table,
            each [FISCAL_YEAR] = FISCAL_YEAR
        ),
    #"Changed column type" =
        Table.TransformColumnTypes(
            #"Filtered rows",
            {
                {
                    "GL_TRANSACTION_PK",
                    Int64.Type
                },
                {
                    "GL_ACCOUNT_FK",
                    Int64.Type
                },
                {
                    "GEOGRAPHY_FK",
                    Int64.Type
                },
                {
                    "PROFIT_CENTER_FK",
                    Int64.Type
                },
                {
                    "COST_CENTER_FK",
                    Int64.Type
                },
                {
                    "COMPANY_CODE",
                    Int64.Type
                },
                {
                    "POSTING_DATE_FK",
                    Int64.Type
                },
                {
                    "DOCUMENT_DATE_FK",
                    Int64.Type
                },
                {
                    "COMPANY_CODE_CURRENCY_VALUE",
                    Currency.Type
                },
                {
                    "GL_ACCOUNT",
                    Int64.Type
                },
                {
                    "GL_AMOUNT",
                    Currency.Type
                }
            }
        )
in
    #"Changed column type"