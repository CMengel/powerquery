let
    // I. Connection to data source
    Source =
        Snowflake.Databases(
            CURRENT_SERVER,
            CURRENT_WAREHOUSE,
            [
                Role = CURRENT_ROLE,
                CreateNavigationProperties = null,
                ConnectionTimeout = null,
                CommandTimeout = null
            ]
        ),
    CTE_PRE_PROD_DB_Database =
        Source{[
            Name = CURRENT_DATABASE,
            Kind = "Database"
        ]}
            [Data],
    CTE_DW_SCHEMA_Schema =
        CTE_PRE_PROD_DB_Database{[
            Name = CURRENT_SCHEMA,
            Kind = "Schema"
        ]}
            [Data],
    V_FACT_INVOICE_View =
        CTE_DW_SCHEMA_Schema{[
            Name = "V_FACT_INVOICE",
            Kind = "View"
        ]}
            [Data],
    V_FACT_INVOICE_ =
        #"12808c91-2cb7-40c4-a7ba-5ebb4693bc22"{[
            entity = "V_FACT_INVOICE",
            version = ""
        ]}
            [Data],
    // II. Specify the desired columns to import from the warehouse
    Select_Columns =
        Table.SelectColumns(
            V_FACT_INVOICE_View,
            {
                "DATE_PK",
                "DATE_KEY",
                "DAY_OF_WEEK",
                "DAY_OF_WEEK_NUM",
                "DAY_OF_MONTH_NUM",
                "DAY_OF_YEAR_NUM",
                "MONTH_NUM",
                "MONTH_NAME",
                "YEAR_NUM",
                "MONTH_YEAR_NUM",
                "WEEK_OF_YEAR_NUM",
                "WEEK_OF_MONTH_NUM",
                "ISWEEKEND",
                "US_HOLIDAY",
                "QUARTER_OF_YEAR_NUM",
                "QUARTER_NAME",
                "LAST_DAY_OF_MONTH",
                "INVOICE_PK",
                "EMPLOYEE_FK",
                "EQUIPMENT_FK",
                "INVOICE_HEADER_DATE_FK",
                "CUSTOMER_SOLD_TO_FK",
                "CUSTOMER_SHIP_TO_FK",
                "GEOGRAPHY_FK",
                "DIVISION_FK",
                "CONTRACT_FK",
                "INVOICE_TYPE",
                "RATE_USED_TYPE",
                "INVOICE_NUM",
                "INVOICE_LINE_ITEM_NUM",
                "INVOICE_LINE_ITEM_UNIT_PRICE",
                "INVOICE_LINE_ITEM_UNIT_ACTUAL_PRICE",
                "INVOICE_LINE_ITEM_UNIT_COUNT",
                "INVOICE_LINE_ITEM_EXTENDED_AMOUNT",
                "INVOICE_LINE_ITEM_EXTENDED_ACTUAL_AMOUNT",
                "INVOICE_LINE_ITEM_TAX",
                "INVOICE_LINE_ITEM_TOTAL_INVOICE_VALUE",
                "INVOICE_LINE_ITEM_TYPE",
                "INVOICE_LINE_ITEM_DESCRIPTION",
                "INVOICE_RATE_SHIFT",
                "IS_RERENT_IND",
                "IS_SUBSTITUTE_IND",
                "IS_ACTIVE",
                "MATERIAL",
                "DIVISION_PK",
                "SUB_DIVISION_CD"
            }
        ),
    // III. Filter, Remove Rows, Replace Values
    #"Filtered rows" =
        Table.SelectRows(
            Select_Columns,
            each [DATE_KEY] > #date(2018, 12, 31)
        )
in
    #"Filtered rows"