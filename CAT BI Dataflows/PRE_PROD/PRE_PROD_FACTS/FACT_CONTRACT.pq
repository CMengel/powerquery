let
    // I. Connection to data source
    Source =
        Snowflake.Databases(
            CURRENT_SOURCE,
            CURRENT_WAREHOUSE,
            [
                Role = CURRENT_ROLE,
                CreateNavigationProperties = null,
                ConnectionTimeout = null,
                CommandTimeout = null
            ]
        ),
    CTE_DB_DATABASE_Database =
        Source{[
            Name = CURRENT_DATABASE,
            Kind = "Database"
        ]}
            [Data],
    CTE_DW_SCHEMA_Schema =
        CTE_DB_DATABASE_Database{[
            Name = CURRENT_SCHEMA,
            Kind = "Schema"
        ]}
            [Data],
    // II. Specify the desired columns to import from the warehouse
    FACT_CONTRACT_Table =
        Table.SelectColumns(
            CTE_DW_SCHEMA_Schema{[
                Name = "FACT_CONTRACT",
                Kind = "Table"
            ]}
                [Data],
            {
                "CONTRACT_PK",
                "EQUIPMENT_FK",
                "EMPLOYEE_FK",
                "GEOGRAPHY_FK",
                "CONTRACT_ID",
                "CONTRACT_LINE_ITEM",
                "CUSTOMER_SHIP_TO_FK",
                "CUSTOMER_BILL_TO_FK",
                "BILLING_INTERVAL",
                "NET_RATE",
                "APPLICABLE_LIST_RATE",
                "DAY_INTERVAL_HOURS_ALLOWED",
                "WEEK_INTERVAL_HOURS_ALLOWED",
                "FOUR_WEEK_INTERVAL_HOURS_ALLOWED",
                "MONTH_INTERVAL_HOURS_ALLOWED",
                "HOURLY_OVERTIME_RATE",
                "OVERTIME_INTERVAL",
                "CONTRACT_STATUS",
                "EQUIPMENT_ADDED_DATE_FK",
                "EQUIPMENT_REMOVED_DATE_FK",
                "LIST_DAY_RATE",
                "LIST_WEEKLY_RATE",
                "LIST_4WEEK_RATE",
                "LIST_MONTHLY_RATE",
                "ON_HIRE_DATE_FK",
                "DELIVERY_DATE_FK",
                "OFF_HIRE_DATE_FK",
                "CONTRACT_SUB_TYPE",
                "SUBSTITUTE_IND",
                "CONTRACT_TYPE",
                "RESERVATION_ID",
                "RERENT_IND",
                "CONVERTED_DATE_FK",
                "RETURNED_DATE_FK",
                "PO_NUMBER",
                "CONTRACT_RATE_SHIFT",
                "SRC_TYP",
                "IS_ACTIVE",
                "CRTD_DTTM"
            }
        ),
    #"Filtered rows" =
        Table.SelectRows(
            FACT_CONTRACT_Table,
            each [SRC_TYP] = "AS400"
        )
in
    #"Filtered rows"