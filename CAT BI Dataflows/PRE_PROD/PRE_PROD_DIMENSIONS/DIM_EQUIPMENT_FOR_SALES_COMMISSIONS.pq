//  2024-10-10: View created to support Commissions automation and incorporate the variety of business rules
//  NOTE: this version is coming from the PROD database and certain parameters are hard coded vs.
//  relying on the parameters in the dataflow
let
    // I. Connection to data source
    Source = Snowflake.Databases(CURRENT_SERVER, CURRENT_WAREHOUSE, [Role = CURRENT_ROLE]),
    CTE_DEV_DB_Database = Source{[Name = "CTE_CAT_PROD_DB", Kind = "Database"]}[Data],
    CTE_BI_SCHEMA_Schema = CTE_DEV_DB_Database{[Name = "CTE_CAT_BI_SCHEMA", Kind = "Schema"]}[Data],
    DIM_EQUIPMENT_FOR_SALES_COMMISSION_View = CTE_BI_SCHEMA_Schema{
        [Name = "DIM_EQUIPMENT_FOR_SALES_COMMISSION", Kind = "View"]
    }[Data],
    Import_Selected_Cols = Table.SelectColumns(
        DIM_EQUIPMENT_FOR_SALES_COMMISSION_View,
        {
            "ACQUISITION_COST",
            "ACQUISITION_COST_ADJUSTMENTS",
            "ACQUISITION_COST_ATTACHMENT",
            "ACQUISITION_COST_FREIGHT",
            "ACQUISITION_COST_MAKE_READY",
            "ACQUISITION_COST_WARRANTY",
            "ACQUISITION_DATE",
            "ACQUISITION_DATE_FK",
            "ANALYSIS_GROUP",
            "COMMISSION_CALC_TYPE_ON_SALE",
            "COMMISSION_PCT_TO_PAY_ON_SALE",
            "CRTD_BY",
            "CRTD_DTTM",
            "CUSTOMER_EQUIPMENT_NUM",
            "CUSTOMER_FK",
            "CUSTOMER_ID",
            "DIVISION_FK",
            "EQUIPMENT_ASKING",
            "EQUIPMENT_BASE_MODEL",
            "EQUIPMENT_CATEGORY_GROUP_DESCRIPTION",
            "EQUIPMENT_CATEGORY_GROUP_NUM",
            "EQUIPMENT_CATEGORY_PRODUCT_LINE",
            "EQUIPMENT_CLASS",
            "EQUIPMENT_COMPATABILITY_MODEL",
            "EQUIPMENT_DISPLAY_MODEL",
            "EQUIPMENT_FAMILY",
            "EQUIPMENT_FMV",
            "EQUIPMENT_FULL_MODEL",
            "EQUIPMENT_IACCOUNT",
            "EQUIPMENT_IACCOUNT_NAME",
            "EQUIPMENT_ID",
            "EQUIPMENT_MAKE",
            "EQUIPMENT_MANUFACTURED_DATE",
            "EQUIPMENT_PK",
            "EQUIPMENT_STATUS",
            "EQUIPMENT_TYPE",
            "FROM_CAROLINACAT_INVENTORY",
            "HASATTACHMENT",
            "INVENTORY_TYPE",
            "IS_ACTIVE",
            "IS_DELETED_IN_SOURCE",
            "IS_EQ_ID_DUMMY",
            "ISATTACHMENT",
            "ISRERENT",
            "LKP_SER_NUMS_CAT_FK",
            "MD5",
            "PRODUCT_LINE_LEVEL_1",
            "PRODUCT_LINE_LEVEL_2",
            "PRODUCT_TYPE",
            "SERIAL_NUM",
            "SRC_SUB_TYP",
            "SRC_TYP",
            "TOTAL_ACQUISITION_COST",
            "UPDT_BY",
            "UPDT_DTTM"
        }
    )
    // III. Filter, Remove Rows, Replace Values
    // Exclude records from the data where WO_LINE_ITEM_DATE >= Parameter
in
    Import_Selected_Cols
