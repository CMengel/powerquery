let
    //  I. Connection to data source
    Source = PowerPlatform.Dataflows(null),
    Workspaces = Source{[Id = "Workspaces"]}[Data],
    #"82cfb7cb-e12a-4001-96b4-c65e3395e24c" = Workspaces{[workspaceId = "82cfb7cb-e12a-4001-96b4-c65e3395e24c"]}[Data],
    #"59d5f4e4-34bf-4e41-aa2f-b78db2eafc44" = #"82cfb7cb-e12a-4001-96b4-c65e3395e24c"{[dataflowId = "59d5f4e4-34bf-4e41-aa2f-b78db2eafc44"]}[Data],
    DIM_EQUIPMENT_ =
        #"59d5f4e4-34bf-4e41-aa2f-b78db2eafc44"{[
            entity = "DIM_EQUIPMENT",
            version = ""
        ]}
            [Data],
    // II. Specify the desired columns to import from the warehouse
    Import_Selected_Cols =
        Table.SelectColumns(
            DIM_EQUIPMENT_,
            {
                "EQUIPMENT_PK",
                "DIVISION_FK",
                "EQUIPMENT_FAMILY",
                "EQUIPMENT_MAKE",
                "EQUIPMENT_TYPE",
                "EQUIPMENT_ID",
                "EQUIPMENT_MODEL",
                "EQUIPMENT_CLASS",
                "EQUIPMENT_CATEGORY_PRODUCT_LINE",
                "PRODUCT_TYPE",
                "SERIAL_NUM",
                "EQUIPMENT_CATEGORY_GROUP_DESCRIPTION",
                "EQUIPMENT_STATUS",
                "ISRERENT",
                "EQUIPMENT_CATEGORY_GROUP_NUM",
                "INVENTORY_TYPE",
                "FULL_EQUIPMENT_MODEL",
                "ACQUISITION_COST",
                "ACQUISITION_DATE",
                "ACQUISITION_COST_ADJUSTMENTS",
                "ACQUISITION_COST_FREIGHT",
                "ACQUISITION_COST_MAKE_READY",
                "ACQUISITION_COST_ATTACHMENT",
                "ACQUISITION_COST_WARRANTY",
                "ACQUISITION_DATE_FK"
            }
        ),
    // III. Filter, Remove Rows, Replace Values, Custom Columns
    #"Duplicated Column" =
        Table.DuplicateColumn(
            Import_Selected_Cols,
            "EQUIPMENT_CATEGORY_GROUP_DESCRIPTION",
            "EQUIPMENT_CATEGORY_GROUP_DESCRIPTION - Copy"
        ),
    #"Renamed Columns" =
        Table.RenameColumns(
            #"Duplicated Column",
            {
                {
                    "EQUIPMENT_CATEGORY_GROUP_DESCRIPTION - Copy",
                    "ShortName_EquipCATGroup"
                }
            }
        ),
    #"Split Column by Delimiter" =
        Table.SplitColumn(
            #"Renamed Columns",
            "ShortName_EquipCATGroup",
            Splitter.SplitTextByEachDelimiter(
                {" "},
                QuoteStyle.Csv,
                false
            ),
            {
                "ShortName_EquipCATGroup.1",
                "ShortName_EquipCATGroup.2"
            }
        ),
    #"Changed Type" =
        Table.TransformColumnTypes(
            #"Split Column by Delimiter",
            {
                {
                    "ShortName_EquipCATGroup.1",
                    type text
                },
                {
                    "ShortName_EquipCATGroup.2",
                    type text
                }
            }
        ),
    #"Split Column by Delimiter1" =
        Table.SplitColumn(
            #"Changed Type",
            "ShortName_EquipCATGroup.2",
            Splitter.SplitTextByEachDelimiter(
                {" "},
                QuoteStyle.Csv,
                false
            ),
            {
                "ShortName_EquipCATGroup.2.1",
                "ShortName_EquipCATGroup.2.2"
            }
        ),
    #"Changed Type1" =
        Table.TransformColumnTypes(
            #"Split Column by Delimiter1",
            {
                {
                    "ShortName_EquipCATGroup.2.1",
                    type text
                },
                {
                    "ShortName_EquipCATGroup.2.2",
                    type text
                }
            }
        ),
    #"Renamed Columns1" =
        Table.RenameColumns(
            #"Changed Type1",
            {
                {
                    "ShortName_EquipCATGroup.1",
                    "Split1_EquipCATGroup"
                },
                {
                    "ShortName_EquipCATGroup.2.1",
                    "Spilt2_EquipCATGroup"
                },
                {
                    "ShortName_EquipCATGroup.2.2",
                    "Spilt3_EquipCATGroup"
                }
            }
        ),
    #"Merged Columns" =
        Table.CombineColumns(
            #"Renamed Columns1",
            {
                "Split1_EquipCATGroup",
                "Spilt2_EquipCATGroup"
            },
            Combiner.CombineTextByDelimiter(
                " ",
                QuoteStyle.None
            ),
            "Equipement_CATGroup_ShortName"
        ),
    #"Renamed Columns2" =
        Table.RenameColumns(
            #"Merged Columns",
            {
                {
                    "Equipement_CATGroup_ShortName",
                    "Equipment_ShortName_CATGroup"
                }
            }
        ),
    // IV. Select the data types for each column according to best practices
    //  Maximize performance, efficiency and accuracy of the data model
    #"Changed Type2" =
        Table.TransformColumnTypes(
            #"Renamed Columns2",
            {
                {
                    "EQUIPMENT_PK",
                    Int64.Type
                },
                {
                    "DIVISION_FK",
                    Int64.Type
                },
                {
                    "ACQUISITION_DATE_FK",
                    Int64.Type
                }
            }
        )
in
    #"Changed Type2"