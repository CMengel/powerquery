//=== Header Block ===
//  Purpose: support Machine Sales Rep Scorecard
//  Query: FACT_EQUIPMENT_QUOTE_PACKAGE_HEADERS
//  Initial Build Date: 9-27-2023
//  Parameters: None
//  Original Author: Chris Mengel
let
    // I. Connection to data source
    Source = PowerPlatform.Dataflows(null),
    Workspaces = Source{[Id = "Workspaces"]}[Data],
    #"786edb99-c918-4c28-8030-cdf1c72872e6" = Workspaces{
        [workspaceId = "786edb99-c918-4c28-8030-cdf1c72872e6"]
    }[Data],
    #"abd5a2fb-d49c-475a-b70b-e35161b07eb6" = #"786edb99-c918-4c28-8030-cdf1c72872e6"{
        [dataflowId = "abd5a2fb-d49c-475a-b70b-e35161b07eb6"]
    }[Data],
    FACT_EQUIPMENT_QUOTE_PACKAGE_HEADERS_ = #"abd5a2fb-d49c-475a-b70b-e35161b07eb6"{
        [entity = "FACT_EQUIPMENT_QUOTE_PACKAGE_HEADERS", version = ""]
    }[Data],
    // II. Specify the desired columns to import from the warehouse
    Select_Cols = Table.SelectColumns(
        FACT_EQUIPMENT_QUOTE_PACKAGE_HEADERS_,
        {
            "ACTUAL_COST",
            "ACTUAL_GP_AMT",
            "ACTUAL_GP_PCT",
            "ACTUAL_SELL",
            "CAT_DISCOUNT_PERCENT",
            "CHANGE_DATE",
            "CUSTOMER_FK",
            "CUSTOMER_ID",
            "DBS_EQUIPMENT_STATUS",
            "DBS_INVENTORY_STATUS",
            "DEAL_CANCELLED",
            "DEAL_SHEET_COST",
            "DEAL_SHEET_GP_AMT",
            "DEAL_SHEET_GP_PCT",
            "DEAL_SHEET_SELL",
            "DIVISION_ID",
            "ENTER_DATE",
            "ENTER_DATE_FK",
            "EQUIP_MAKEMODEL_CATEGORY_LOOKUP_FK",
            "EQUIPMENT_STATUS",
            "GRAND_TOTAL_INCL_WARRANTY_CSA",
            "IS_ACTIVE",
            "IS_CURRENT_REVISION",
            "LAST_CHANGED_DATE_FK",
            //"PACKAGE_ID",
            "PACKAGE_MAKE",
            "PACKAGE_MODEL",
            //"PACKAGE_NAME",
            "QUOTE_BASE_MODEL",
            "QUOTE_DEFAULT_GP_PERCENT",
            "QUOTE_EQUIPMENT_FK",
            "QUOTE_EQUIPMENT_ID",
            "QUOTE_GP_PERCENT",
            "QUOTE_MAKE",
            "QUOTE_MODEL",
            "QUOTE_NUM",
            "QUOTE_PKG_ENTERED_BY",
            "QUOTE_PKG_ENTERED_BY_USER_ID",
            "QUOTE_PKG_LAST_CHAGNED_BY_USER_ID",
            "QUOTE_PKG_LAST_CHANGED_BY",
            "QUOTE_STATUS",
            "RESTATED_COST",
            "RESTATED_GP_AMT",
            "RESTATED_GP_PCT",
            "RESTATED_SELL",
            "REVISION_NUM",
            "REVISION_STATUS",
            "SALES_AGREEMENT_NUM",
            "SALES_INVOICE_DATE",
            "SALES_INVOICE_NUM",
            "SALES_REP_FK",
            "SALES_REP_ID",
            "SOLD_EQUIPMENT_ID",
            "SOURCE",
            "TOTAL_EXCL_WARRANTY_CSA"
        }
    ),
    //  III.    Filter desired rows, transform data, calculated columns, etc.
    //  Create a custom date range to implement a rolling window of time and only import
    //  records within that custom date range => prevent the model from growing too large
    Start_of_Custom_Date_Range =
        let
            Start = Date.From(DateTime.LocalNow()),
            Move_Back_Num_Years = Date.AddYears(Start, (NUMBER_OF_YEARS * -1)),
            Start_of_Year = Date.StartOfYear(Move_Back_Num_Years)
        in
            Start_of_Year,
    Filter_IS_Active = Table.SelectRows(Select_Cols, each [IS_ACTIVE] = "Y"),
    #"Split Column by Delimiter" = Table.SplitColumn(
        Table.TransformColumnTypes(Filter_IS_Active, {{"CHANGE_DATE", type text}}, "en-US"),
        "CHANGE_DATE",
        Splitter.SplitTextByEachDelimiter({" "}, QuoteStyle.Csv, false),
        {"CHANGE_DATE.1", "CHANGE_DATE.2"}
    ),
    #"Changed Type" = Table.TransformColumnTypes(
        #"Split Column by Delimiter",
        {
            {"CHANGE_DATE.1", type date},
            {"CHANGE_DATE.2", type time},
            {"QUOTE_NUM", Int64.Type},
            {"IS_CURRENT_REVISION", type text}
        }
    ),
    #"Split Column by Delimiter1" = Table.SplitColumn(
        Table.TransformColumnTypes(#"Changed Type", {{"ENTER_DATE", type text}}, "en-US"),
        "ENTER_DATE",
        Splitter.SplitTextByEachDelimiter({" "}, QuoteStyle.Csv, false),
        {"ENTER_DATE.1", "ENTER_DATE.2"}
    ),
    #"Changed Type1" = Table.TransformColumnTypes(
        #"Split Column by Delimiter1", {{"ENTER_DATE.1", type date}, {"ENTER_DATE.2", type time}}
    ),
    Filter_Enter_Date = Table.SelectRows(#"Changed Type1", each [ENTER_DATE.1] >= Start_of_Custom_Date_Range),
    #"Removed Columns" = Table.RemoveColumns(
        Filter_Enter_Date, {"CHANGE_DATE.2", "ENTER_DATE.2", "DIVISION_ID", "IS_ACTIVE"}
    ),
    #"Renamed Columns" = Table.RenameColumns(
        #"Removed Columns", {{"CHANGE_DATE.1", "CHANGE_DATE"}, {"ENTER_DATE.1", "ENTER_DATE"}}
    ),
    Add_Invoice_Indicator = Table.AddColumn(
        #"Renamed Columns",
        "INVOICE_INDICATOR",
        each if [SALES_INVOICE_DATE] = null and [SALES_INVOICE_NUM] = "" then "N" else "Y",
        type text
    ),
    #"Filtered Rows" = Table.SelectRows(Add_Invoice_Indicator, each true),
    #"Merged Queries" = Table.NestedJoin(
        #"Filtered Rows",
        {"EQUIP_MAKEMODEL_CATEGORY_LOOKUP_FK"},
        DIM_EQUIPMENT_MAKE_MODEL_CATEGORY_LOOKUP,
        {"EQUIPMENT_MAKE_MODEL_PK"},
        "DIM_EQUIPMENT_MAKE_MODEL_CATEGORY_LOOKUP",
        JoinKind.LeftOuter
    ),
    #"Expanded DIM_EQUIPMENT_MAKE_MODEL_CATEGORY_LOOKUP" = Table.ExpandTableColumn(
        #"Merged Queries",
        "DIM_EQUIPMENT_MAKE_MODEL_CATEGORY_LOOKUP",
        {"PRODUCT_LINE_LEVEL_1", "PRODUCT_LINE_LEVEL_2"},
        {"PRODUCT_LINE_LEVEL_1", "PRODUCT_LINE_LEVEL_2"}
    )
in
    #"Expanded DIM_EQUIPMENT_MAKE_MODEL_CATEGORY_LOOKUP"
