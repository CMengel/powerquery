//=== Header Block ===
//  Purpose: Aggregate YTD activities at the Parent level
//      and the Assigned Rep Level.  This data is used to compute PAR Status for the Parent Customer.
//      This data is essential to compute Coverage Metrics such as # Accounts Covered, Under Covered, No Coverage, etc.
//  Query: base query is FACT_SALES_ACTIVITY
//  Initial Build Date: 9-9-2024
//  Parameters: None
//  Original Author: Chris Mengel
//
let
    // I. Connection to data source
    Source = PowerPlatform.Dataflows(null),
    Workspaces = Source{[Id = "Workspaces"]}[Data],
    #"82cfb7cb-e12a-4001-96b4-c65e3395e24c" = Workspaces{
        [workspaceId = "82cfb7cb-e12a-4001-96b4-c65e3395e24c"]
    }[Data],
    #"3a487aec-9add-441f-a6a7-06474f81978b" = #"82cfb7cb-e12a-4001-96b4-c65e3395e24c"{
        [dataflowId = "3a487aec-9add-441f-a6a7-06474f81978b"]
    }[Data],
    FACT_SALES_ACTIVITY_ = #"3a487aec-9add-441f-a6a7-06474f81978b"{
        [entity = "FACT_SALES_ACTIVITY", version = ""]
    }[Data],
    // II. Specify the desired columns to import from the warehouse
    FACT_SALES_ACTIVITY_Table = Table.SelectColumns(
        FACT_SALES_ACTIVITY_,
        {"ACTIVITY_ID", "STATUS_NAME", "DIVISION_FK", "ACTIVITY_DATE", "CUSTOMER_ID", "SALES_REP_ID", "IS_ACTIVE"}
    ),
    #"Renamed Columns" = Table.RenameColumns(
        FACT_SALES_ACTIVITY_Table, {{"SALES_REP_ID", "ACTY_SALES_REP_ID"}, {"CUSTOMER_ID", "ACTY_CUSTOMER_ID"}}
    ),
    // III. Filter, Remove Rows, Replace Values, Custom Columns
    //  Create a custom date range to implement a rolling window of time and only import
    //  records within that custom date range => prevent the model from growing too large
    Start_of_Year_Value =
        let
            Start = Date.From(DateTime.LocalNow()), Start_of_Year = Date.StartOfYear(Start)
        in
            Start_of_Year,
    //  Filter active Construction records, within the current year that are status of completed
    Filter_IS_Active = Table.SelectRows(
        #"Renamed Columns",
        each
            [IS_ACTIVE] = "Y"
            and [STATUS_NAME] = "Completed"
            and [ACTIVITY_DATE] >= Start_of_Year_Value
            and [DIVISION_FK] = 9
    ),
    #"Merged Queries" = Table.NestedJoin(
        Filter_IS_Active,
        {"ACTY_CUSTOMER_ID"},
        DIM_CUSTOMER_DIVISION,
        {"CUSTOMER_ID"},
        "DIM_CUSTOMER_DIVISION",
        JoinKind.LeftOuter
    ),
    #"Expanded DIM_CUSTOMER_DIVISION" = Table.ExpandTableColumn(
        #"Merged Queries",
        "DIM_CUSTOMER_DIVISION",
        {"MSR_SALES_REP_ID", "CUSTOMER_TYPE", "CUSTOMER_PARENT_ID"},
        {"MSR_SALES_REP_ID", "CUSTOMER_TYPE", "CUSTOMER_PARENT_ID"}
    ),
    #"Filtered Rows" = Table.SelectRows(
        #"Expanded DIM_CUSTOMER_DIVISION",
        //  2024-08-07: compute for both External Customers and Prospects
        each ([CUSTOMER_TYPE] = "External" or [CUSTOMER_TYPE] = "Prospect")
    ),
    Add_YTD_Sales_Rep_col = Table.AddColumn(
        #"Filtered Rows",
        "YTD_PARENT_LEVEL_ACTIVITIES_ASND_SALES_REP",
        each if [ACTY_SALES_REP_ID] = [MSR_SALES_REP_ID] then 1 else 0,
        Int64.Type
    ),
    #"Removed Columns" = Table.RemoveColumns(
        Add_YTD_Sales_Rep_col,
        {
            "ACTIVITY_ID",
            "STATUS_NAME",
            "ACTIVITY_DATE",
            "ACTY_CUSTOMER_ID",
            "ACTY_SALES_REP_ID",
            "IS_ACTIVE",
            "CUSTOMER_TYPE",
            "MSR_SALES_REP_ID"
        }
    ),
    #"Grouped Rows" = Table.Group(
        #"Removed Columns",
        {"CUSTOMER_PARENT_ID"},
        {
            {
                "YTD_PARENT_LEVEL_ACTY_ASND_SALES_REP",
                each List.Sum([YTD_PARENT_LEVEL_ACTIVITIES_ASND_SALES_REP]),
                type number
            }
        }
    )
in
    #"Grouped Rows"