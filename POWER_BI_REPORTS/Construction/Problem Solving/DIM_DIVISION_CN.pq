//=== Header Block ===
//  Purpose: support Problem Solving for Construction Service Operations
//  Query: DIM_DIVISION_CN
//  Initial Build Date: 8-17-2021
//  Parameters:
//      CURRENT_WAREHOUSE
//      CURRENT_ROLE
//      CURRENT_DATABASE
//      CURRENT_SCHEMA
//  Original Author: Chris Mengel
//
let
    //  I. Connection to data source
    Source = PowerPlatform.Dataflows(null),
    Workspaces = Source{[Id = "Workspaces"]}[Data],
    #"82cfb7cb-e12a-4001-96b4-c65e3395e24c" = Workspaces{
        [workspaceId = "82cfb7cb-e12a-4001-96b4-c65e3395e24c"]
    }[Data],
    #"59d5f4e4-34bf-4e41-aa2f-b78db2eafc44" = #"82cfb7cb-e12a-4001-96b4-c65e3395e24c"{
        [dataflowId = "59d5f4e4-34bf-4e41-aa2f-b78db2eafc44"]
    }[Data],
    DIM_DIVISION_ = #"59d5f4e4-34bf-4e41-aa2f-b78db2eafc44"{[
        entity = "DIM_DIVISION",
        version = ""
    ]}[Data],
    // II.  Specify the desired columns to import from the warehouse
    DIM_DIVISION_Table = Table.SelectColumns(
        DIM_DIVISION_, {"DIVISION_PK", "DIVISION_NAME", "SUB_DIVISION_NAME", "SUB_DIVISION_CD", "IS_ACTIVE"}
    ),
    // III. Filter, Remove Rows, Replace Values, Custom Columns
    Import_Construction_Data = Table.SelectRows(
        DIM_DIVISION_Table,
        each List.Contains({
            "P",
            //  Forestry
            "G",
            //  Construction Equip
            "S",
            //  Paving
            "T",
            //  Truck (includes EVS)
            "K",
            //  Southern Vac
            "H"
            // Construction Technology added 2-21-2023
        }, [SUB_DIVISION_CD]) and [IS_ACTIVE] = "Y"
    ),
    // IV. Selct the data types for each column according to best practices
    //  Maximize performance, efficiency and accuracy of the data model
    #"Changed Type" = Table.TransformColumnTypes(
        Import_Construction_Data,
        {
            {"DIVISION_PK", Int64.Type},
            {"DIVISION_NAME", type text},
            {"SUB_DIVISION_NAME", type text},
            {"SUB_DIVISION_CD", type text}
        }
    ),
    #"Replaced Value" = Table.ReplaceValue(
        #"Changed Type", "CONSTRUCTION TECHNOLOGY", "CE TECHNOLOGY", Replacer.ReplaceText, {"SUB_DIVISION_NAME"}
    )
in
    #"Replaced Value"