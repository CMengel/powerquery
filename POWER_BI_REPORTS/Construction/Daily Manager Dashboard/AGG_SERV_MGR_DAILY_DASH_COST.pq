//=== Header Block ===
//  Purpose: support Daily Manager Dashboard for Construction Service
//  Query: AGG_SERV_MGR_DAILY_DASH_COST
//  Initial Build Date: 5-23-2022
//  Parameters:
//      CURRENT_WAREHOUSE
//      CURRENT_ROLE
//      CURRENT_DATABASE
//      CURRENT_SCHEMA
//  Original Author: Chris Mengel
//
let
    //  I. Connection to data source
    Source = PowerPlatform.Dataflows(
        null
    ),
    Workspaces = Source{
        [
            Id = "Workspaces"
        ]
    }[
        Data
    ],
    #"82cfb7cb-e12a-4001-96b4-c65e3395e24c" = Workspaces{
        [
            workspaceId = "82cfb7cb-e12a-4001-96b4-c65e3395e24c"
        ]
    }[
        Data
    ],
    #"74008925-fe4f-4fb8-8c19-dba1ef52e4e0" = #"82cfb7cb-e12a-4001-96b4-c65e3395e24c"{
        [
            dataflowId = "74008925-fe4f-4fb8-8c19-dba1ef52e4e0"
        ]
    }[
        Data
    ],
    AGG_SERV_MGR_DAILY_DASH_COST_ = #"74008925-fe4f-4fb8-8c19-dba1ef52e4e0"{
        [
            entity = "AGG_SERV_MGR_DAILY_DASH_COST",
            version = ""
        ]
    }[
        Data
    ],
    // II. Specify the desired columns to import from the warehouse
    AGG_SERV_MGR_DAILY_DASH_COST_Table = Table.SelectColumns(
        AGG_SERV_MGR_DAILY_DASH_COST_,
        {
                "AGG_SERV_MGR_DAILY_DASH_PK",
                "DATE_FK",
                "GL_PROFIT_CENTER_FK",
                "EXTERNAL_WO_LBR_SELL_AMT",
                "EXTERNAL_WO_HRS",
                "WO_BILLED_EXTERNALINTERNAL_HRS",
                "WO_COSTOFSALES_HRS",
                "WO_RESPREWORK_HRS",
                "WO_NONBILLABLE_HRS",
                "PAID_LESS_ALLOWEDORREWORK_HRS",
                "PAID_ALLOWED_HRS",
                "PAID_REWORK_HRS",
                "SALES_AMT",
                "SALES_GOAL",
                "IS_ACTIVE",
                "TOTAL_IDLE_TIME",
                "TOTAL_NON_CHARGEABLE",
                "TOTAL_TRAINING",
                "TOTAL_TECH_SUPERVISION",
                "TOTAL_SMALL_TOOLING",
                "TOTAL_POLICY",
                "TOTAL_PAD",
                "TOTAL_PBT",
                "TOTAL_REVENUE",
                "TOTAL_COS",
                "TOTAL_TRUCK_REVENUE",
                "TOTAL_TRUCK_COS",
                "TOTAL_LABOR_OVERAGE",
                "TOTAL_INDIRECT_EXP",
                "TOTAL_DIRECT_EXP",
                "TOTAL_OTHER"
        }
    ),
    Add_Tech_Prod_Numerator_Col = Table.AddColumn(
        AGG_SERV_MGR_DAILY_DASH_COST_Table,
        "TECH_PRODUCTIVITY_NUMERATOR",
        each
            [
                WO_BILLED_EXTERNALINTERNAL_HRS
            ] + [
                WO_RESPREWORK_HRS
            ] + [
                WO_COSTOFSALES_HRS
            ],
        type number
    ),
    // IV. Selct the data types for each column according to best practices
    //  Maximize performance, efficiency and accuracy of the data model
    #"Changed Type" = Table.TransformColumnTypes(
        Add_Tech_Prod_Numerator_Col,
        {
            {
                "AGG_SERV_MGR_DAILY_DASH_PK",
                Int64.Type
            },
            {
                "DATE_FK",
                Int64.Type
            },
            {
                "GL_PROFIT_CENTER_FK",
                Int64.Type
            }
        }
    )
in
    #"Changed Type"
