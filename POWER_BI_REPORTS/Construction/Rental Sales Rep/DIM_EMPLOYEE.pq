//=== Header Block ===
//  Purpose: support Technician Productivity for Construction
//  Query: DIM_EMPLOYEE_CN
//  Initial Build Date: 5-6-2021
//  Parameters:
//      CURRENT_WAREHOUSE
//      CURRENT_ROLE
//      CURRENT_DATABASE
//      CURRENT_SCHEMA
//  Original Author: Chris Mengel
let
    //  I. Connection to data source
    Source = PowerPlatform.Dataflows(null),
    Workspaces = Source{[Id = "Workspaces"]}[Data],
    #"4daa2835-744f-4985-b01e-7603509a7ddb" = Workspaces{[workspaceId = "4daa2835-744f-4985-b01e-7603509a7ddb"]}[Data],
    #"38724c86-32cc-4ff9-a5c1-454f098c52a0" = #"4daa2835-744f-4985-b01e-7603509a7ddb"{[dataflowId = "38724c86-32cc-4ff9-a5c1-454f098c52a0"]}[Data],
    DIM_EMPLOYEE_ =
        #"38724c86-32cc-4ff9-a5c1-454f098c52a0"{[
            entity = "DIM_EMPLOYEE",
            version = ""
        ]}
            [Data],
    // II. Specify the desired columns to import from the warehouse
    DIM_EMPLOYEE_Table =
        Table.SelectColumns(
            DIM_EMPLOYEE_,
            {
                "EMPLOYEE_PK",
                "EMPLOYEE_ID",
                "EMPLOYEE_FIRST_NAME",
                "TITLE",
                "MANAGER_NAME",
                "BRANCH",
                "JOB_ROLE",
                "GEOGRAPHY_FK",
                "IS_ACTIVE"
            }
        ),
    // III. Filter, Remove Rows, Replace Values
    //  target only Rental Sales Reps
    Filter_Sales_Reps =
        Table.SelectRows(
            DIM_EMPLOYEE_Table,
            each [TITLE] = "Sales Rep"
        ),
    // assign division based on first character of employee_id
    Add_DIVISION_OF_REP_Col =
        Table.AddColumn(
            Filter_Sales_Reps,
            "DIVISION_OF_REP",
            each
                if Text.StartsWith([EMPLOYEE_ID], "P") then
                    "CONSTRUCTION"
                else if Text.StartsWith([EMPLOYEE_ID], "G") then
                    "CONSTRUCTION"
                else if Text.StartsWith([EMPLOYEE_ID], "S") then
                    "CONSTRUCTION"
                else if Text.StartsWith([EMPLOYEE_ID], "E") then
                    "POWER"
                else
                    "OTHER",
            type text
        ),
    Filter_OTHER_Rows =
        Table.SelectRows(
            Add_DIVISION_OF_REP_Col,
            each ([DIVISION_OF_REP] <> "OTHER")
        ),
    //  Exclude mulitple non-employee type records from the data
    Filtered_NON_EMPLOYEE_Rows =
        Table.SelectRows(
            Filter_OTHER_Rows,
            each not List.Contains(
                {
                    "INACT",
                    "INTER",
                    "SALE ",
                    "SALES",
                    "DO NO",
                    "OUT O",
                    "TRUCK",
                    "TRANS",
                    "HOLDI",
                    "ENGIN",
                    "EMERG",
                    "OPEN",
                    "HYDRA",
                    "CASCA",
                    "CREDI",
                    "MARKE",
                    "PSSR "
                },
                Text.Start([EMPLOYEE_FIRST_NAME], 5)
            )
        ),
    #"Capitalized Each Word" =
        Table.TransformColumns(
            Filtered_NON_EMPLOYEE_Rows,
            {
                {
                    "EMPLOYEE_FIRST_NAME",
                    Text.Proper,
                    type text
                }
            }
        ),
    #"Renamed Columns" =
        Table.RenameColumns(
            #"Capitalized Each Word",
            {
                {
                    "EMPLOYEE_FIRST_NAME",
                    "EMPLOYEE_NAME"
                }
            }
        ), 
    #"Added Conditional Column" = Table.AddColumn(
            #"Renamed Columns",
            "MNGR",
            each
                if [EMPLOYEE_NAME] = "Sherman White" then
                    "Jeff Traina"
                else if [EMPLOYEE_NAME] = "Spencer Sain" then
                    ""
                else if [EMPLOYEE_NAME] = "Zack Harris" then
                    ""
                else if [MANAGER_NAME] = "" 
                    and [EMPLOYEE_NAME] <> "Sherman White" then
                    "Not Traina"
                else if [MANAGER_NAME] = "Jeff Traina" then
                    "Jeff Traina"
                else if [MANAGER_NAME] = "Todd Vallencour" then
                    "Todd Vallencour"
                else
                    [MANAGER_NAME],
            type text
        ),
    // IV. Selct the data types for each column according to best practices
    //  Maximize performance, efficiency and accuracy of the data model
    #"Changed Type" =
        Table.TransformColumnTypes(
            #"Added Conditional Column",
            {
                {
                    "EMPLOYEE_PK",
                    Int64.Type
                },
                {
                    "GEOGRAPHY_FK",
                    Int64.Type
                }
            }
        )
in
    #"Changed Type"