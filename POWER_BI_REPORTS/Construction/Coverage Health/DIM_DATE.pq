//=== Header Block ===
//  Purpose: support Serivce Financials for Construction
//  Query: DIM_DATE
//  Initial Build Date: 5-23-2022
//  Parameters:
//
//  Original Author: Chris Mengel
//
let
    //  I. Connection to data source
    Source = PowerPlatform.Dataflows(null),
    Workspaces = Source{[Id = "Workspaces"]}[Data],
    #"82cfb7cb-e12a-4001-96b4-c65e3395e24c" = Workspaces{
        [workspaceId = "82cfb7cb-e12a-4001-96b4-c65e3395e24c"]
    }[Data],
    #"59d5f4e4-34bf-4e41-aa2f-b78db2eafc44" = #"82cfb7cb-e12a-4001-96b4-c65e3395e24c"{
        [dataflowId = "59d5f4e4-34bf-4e41-aa2f-b78db2eafc44"]
    }[Data],
    DIM_DATE_ = #"59d5f4e4-34bf-4e41-aa2f-b78db2eafc44"{[entity = "DIM_DATE", version = ""]}[Data],
    // II. Specify the desired columns to import from the warehouse
    DIM_DATE_Table = Table.SelectColumns(
        DIM_DATE_,
        {
            "DATE_PK",
            "DATE_KEY",
            "DAY_OF_WEEK",
            "DAY_OF_WEEK_NUM",
            "DAY_OF_MONTH_NUM",
            "DAY_OF_YEAR_NUM",
            "MONTH_NUM",
            "MONTH_NAME",
            "YEAR_NUM",
            "MONTH_YEAR_NUM",
            "WEEK_OF_YEAR_NUM",
            "WEEK_OF_MONTH_NUM",
            "QUARTER_OF_YEAR_NUM",
            "QUARTER_NAME",
            "ISWEEKEND",
            "US_HOLIDAY",
            "CTE_HOLIDAY",
            "LAST_DAY_OF_MONTH",
            "QUARTER_YEAR",
            "SORT_QUARTER_YEAR",
            "MONTH_YEAR",
            "SORT_MONTH_YEAR",
            "MONTH_WEEK_YEAR",
            "SORT_MONTH_WEEK_YEAR",
            "WEEK_YEAR",
            "SORT_WEEK_YEAR",
            "IS_WORKDAY"
        }
    ),
    // III. Filter, Remove Rows, Replace Values, Add Custom Columns
    #"Filtered Rows" = Table.SelectRows(
        DIM_DATE_Table,
        each
        //  2024-07-30: revise date table to include all the complete years of the Customer Fleet Table
        //  replace rolling 5 year logic with a start date query
        //    [YEAR_NUM] >= Date.Year(Date.From(Date.EndOfYear(DateTime.LocalNow()))) - NUMBER_OF_YEARS
        [DATE_KEY] >= START_DATE and [DATE_KEY] <= Date.From(Date.EndOfYear(DateTime.LocalNow()))
    ),
    //  Add column for the number of calendar days in a month
    Add_Days_In_Month_Col = Table.AddColumn(
        #"Filtered Rows", "DAYS_IN_MONTH", each Date.DaysInMonth([DATE_KEY]), Int64.Type
    ),
    //  Add column to group rolling 5 years to be used as a slicer in the report to aid usability
    Add_Five_Years_Col = Table.AddColumn(
        Add_Days_In_Month_Col,
        "ROLLING_FIVE_YEARS",
        each
            if [YEAR_NUM] >= Date.Year(Date.From(Date.EndOfYear(DateTime.LocalNow()))) - NUMBER_OF_YEARS then
                "5 Years"
            else
                "All Other Years"
    ),
    // IV. Selct the data types for each column according to best practices
    //  Maximize performance, efficiency and accuracy of the data model
    #"Changed Type" = Table.TransformColumnTypes(
        Add_Five_Years_Col,
        {
            {"DATE_PK", Int64.Type},
            {"DATE_KEY", type date},
            {"DAY_OF_WEEK", type text},
            {"DAY_OF_WEEK_NUM", Int64.Type},
            {"DAY_OF_MONTH_NUM", Int64.Type},
            {"DAY_OF_YEAR_NUM", Int64.Type},
            {"MONTH_NUM", Int64.Type},
            {"YEAR_NUM", Int64.Type},
            {"QUARTER_NAME", type text},
            {"ISWEEKEND", type text},
            {"US_HOLIDAY", type text}
        }
    )
in
    #"Changed Type"
