//=== Header Block ===
//  Purpose: support Customer Fleet Report
//  Query: DIM_CUSTOMER
//  Initial Build Date: 5-6-2021
//  Parameters: None
//  Original Author: Chris Mengel
//
let
    //  I. Connection to data source
    Source = PowerPlatform.Dataflows(null),
    Workspaces = Source{[Id = "Workspaces"]}[Data],
    #"82cfb7cb-e12a-4001-96b4-c65e3395e24c" = Workspaces{
        [workspaceId = "82cfb7cb-e12a-4001-96b4-c65e3395e24c"]
    }[Data],
    #"59d5f4e4-34bf-4e41-aa2f-b78db2eafc44" = #"82cfb7cb-e12a-4001-96b4-c65e3395e24c"{
        [dataflowId = "59d5f4e4-34bf-4e41-aa2f-b78db2eafc44"]
    }[Data],
    DIM_CUSTOMER_ = #"59d5f4e4-34bf-4e41-aa2f-b78db2eafc44"{[entity = "DIM_CUSTOMER", version = ""]}[Data],
    // II. Specify the desired columns to import from the warehouse
    DIM_CUSTOMER_Table = Table.SelectColumns(
        DIM_CUSTOMER_,
        {
            "CUSTOMER_PK",
            "CUSTOMER_ID",
            "CUSTOMER_NAME",
            "CUSTOMER_TYPE",
            "CUSTOMER_PARENT_NAME",
            "CUSTOMER_PARENT_ID",
            "IS_ACTIVE"
        }
    ),
    // III. Filter, Remove Rows, Replace Values, Custom Columns
    //#"Capitalized Each Word" = Table.TransformColumns(DIM_CUSTOMER_Table,{{"CUSTOMER_PARENT_NAME", Text.Proper, type text}}),
    Filter_IS_ACTIVE = Table.SelectRows(DIM_CUSTOMER_Table, each ([IS_ACTIVE] = "Y")),
    //  Determine if report is for external or internal use
    External_Use =
        if EXTERNAL_USE = true then
            Table.SelectRows(Filter_IS_ACTIVE, each [CUSTOMER_PARENT_ID] = PARENT_CUSTOMER_ID)
        else
            Filter_IS_ACTIVE,
    // IV. Selct the data types for each column according to best practices
    //  Maximize performance, efficiency and accuracy of the data model
    #"Changed Type" = Table.TransformColumnTypes(External_Use, {{"CUSTOMER_PK", Int64.Type}}),
    #"Renamed Columns" = Table.RenameColumns(
        #"Changed Type",
        {
            {"CUSTOMER_PK", "PARENT_CUSTOMER_PK"},
            {"CUSTOMER_TYPE", "PARENT_CUSTOMER_TYPE"},
            {"CUSTOMER_PARENT_NAME", "PARENT_CUSTOMER_PARENT_NAME"},
            {"CUSTOMER_PARENT_ID", "PARENT_CUSTOMER_PARENT_ID"}
        }
    )
in
    #"Renamed Columns"