let
    //  I. Connection to data source
    Source = PowerPlatform.Dataflows(null),
    Workspaces = Source{[Id = "Workspaces"]}[Data],
    #"4daa2835-744f-4985-b01e-7603509a7ddb" = Workspaces{
        [workspaceId = "4daa2835-744f-4985-b01e-7603509a7ddb"]
    }[Data],
    #"38724c86-32cc-4ff9-a5c1-454f098c52a0" = #"4daa2835-744f-4985-b01e-7603509a7ddb"{
        [dataflowId = "38724c86-32cc-4ff9-a5c1-454f098c52a0"]
    }[Data],
    DIM_GEOGRAPHY_ = #"38724c86-32cc-4ff9-a5c1-454f098c52a0"{[
        entity = "DIM_GEOGRAPHY",
        version = ""
    ]}[Data],
    // II. Specify the desired columns to import from the warehouse
    DIM_GEOGRAPHY_Table = Table.SelectColumns(
        DIM_GEOGRAPHY_,
        {
            "GEOGRAPHY_PK",
            "BRANCH_ID",
            "BRANCH_NAME",
            "BRANCH_STRING",
            "BRANCH_COST_CENTER",
            "BRANCH_TYPE",
            "IS_ACTIVE"
        }
    ),
    // III. Filter, Remove Rows, Replace Values
    //  Exclude RENTAL branches and SAP records from the data
    //  not needed for tech productivity
    FilteredSERVICE_AND_AS400Rows = Table.SelectRows(
        DIM_GEOGRAPHY_Table, each ([BRANCH_TYPE] = "SERVICE") and [IS_ACTIVE] = "Y"
    ),
    //  Exclude Power Systems and Scitech branches from the data
    FilteredPOWER_SYSTEMSRows = Table.SelectRows(
        FilteredSERVICE_AND_AS400Rows,
        each not List.Contains({
            "77",
            "88",
            "EA",
            "EC",
            "EG",
            "BC",
            "AC"
            //"ST" opened to include SiTech 7-28-2022
        }, [BRANCH_STRING])
    ),
    //  Custom column to group branch strings together in ways business is familiar with;
    //  Used as a filter/slicer in the report
    #"AddedBRANCH_GROUP" = Table.AddColumn(
        FilteredPOWER_SYSTEMSRows,
        "STORE_GROUP",
        each
            if List.Contains({"BC"}, [BRANCH_STRING]) then
                "AES"
            else if List.Contains({"EA", "EC", "EG"}, [BRANCH_STRING]) then
                "EPG"
            else if List.Contains({"DA", "DC", "GL", "ST", "UC"}, [BRANCH_STRING]) then
                "Non-Traditional"
            else if List.Contains({"AC", "SG"}, [BRANCH_STRING]) then
                "OTHER"
            else if List.Contains({"77", "88"}, [BRANCH_STRING]) then
                "PPS"
            else if List.Contains(
                {"CA", "CC", "CG", "CH", "CM", "CN", "CP", "CS", "HA", "HC", "HG", "HH", "HM", "HN"}, [BRANCH_STRING]
            ) then
                "Traditional Service"
            else if List.Contains({"TA", "TC", "TG", "TM", "VA", "VC"}, [BRANCH_STRING]) then
                "Truck/EVS"
            else
                "CLARIFY",
        type text
    ),
    // IV. Selct the data types for each column according to best practices
    //  Maximize performance, efficiency and accuracy of the data model
    #"Changed Type" = Table.TransformColumnTypes(AddedBRANCH_GROUP, {{"GEOGRAPHY_PK", Int64.Type}})
in
    #"Changed Type"