//=== Header Block ===
//  Purpose: support Problem Solving for Construction Service Operations
//  Query: DIM_DIVISION_CN
//  Initial Build Date: 8-17-2021
//  Parameters:
//      CURRENT_WAREHOUSE
//      CURRENT_ROLE
//      CURRENT_DATABASE
//      CURRENT_SCHEMA
//  Original Author: Chris Mengel
//
let
    //  I. Connection to data source
    Source = PowerPlatform.Dataflows(null),
    Workspaces = Source{[Id = "Workspaces"]}[Data],
    #"4daa2835-744f-4985-b01e-7603509a7ddb" = Workspaces{
        [workspaceId = "4daa2835-744f-4985-b01e-7603509a7ddb"]
    }[Data],
    #"38724c86-32cc-4ff9-a5c1-454f098c52a0" = #"4daa2835-744f-4985-b01e-7603509a7ddb"{
        [dataflowId = "38724c86-32cc-4ff9-a5c1-454f098c52a0"]
    }[Data],
    DIM_DIVISION_ = #"38724c86-32cc-4ff9-a5c1-454f098c52a0"{[
        entity = "DIM_DIVISION",
        version = ""
    ]}[Data],
    // II.  Specify the desired columns to import from the warehouse
    DIM_DIVISION_Table = Table.SelectColumns(
        DIM_DIVISION_, {"DIVISION_PK", "DIVISION_NAME", "SUB_DIVISION_NAME", "SUB_DIVISION_CD", "IS_ACTIVE"}
    ),
    // III. Filter, Remove Rows, Replace Values, Custom Columns
    #"Filtered Rows" = Table.SelectRows(
        DIM_DIVISION_Table,
        each List.Contains({
            "P",
            //  Forestry
            "G",
            //  Construction Equip
            "S",
            //  Paving
            "T",
            //  Truck (includes EVS)
            "K",
            //  Southern Vac
            "H"
            // Construction Technology added 2-21-2023
        }, [SUB_DIVISION_CD]) and [IS_ACTIVE] = "Y"
    ),
    // IV. Selct the data types for each column according to best practices
    //  Maximize performance, efficiency and accuracy of the data model
    #"Changed Type" = Table.TransformColumnTypes(
        #"Filtered Rows",
        {
            {"DIVISION_PK", Int64.Type},
            {"DIVISION_NAME", type text},
            {"SUB_DIVISION_NAME", type text},
            {"SUB_DIVISION_CD", type text}
        }
    )
in
    #"Changed Type"
