//=== Header Block ===
//  Purpose: support Policy Allowance for Construction
//  Query:  FACT_WORK_ORDER_SUMMARY_CAT_CN
//  Initial Build Date: 7/7/2021
//  Parameters:
//      CURRENT_WAREHOUSE
//      CURRENT_ROLE
//      CURRENT_DATABASE
//      CURRENT_SCHEMA
//      EARLIEST_WOLI_DATE - limit the # rows    
//  Original Author: Chris Mengel
// 
let
    //  I. Connection to data source
    Source = PowerPlatform.Dataflows(null),
    Workspaces = Source{[Id = "Workspaces"]}[Data],
    #"4daa2835-744f-4985-b01e-7603509a7ddb" = Workspaces{[workspaceId = "4daa2835-744f-4985-b01e-7603509a7ddb"]}[Data],
    #"21a564e6-8785-46ed-bcc3-8a78b226cd0d" = #"4daa2835-744f-4985-b01e-7603509a7ddb"{[dataflowId = "21a564e6-8785-46ed-bcc3-8a78b226cd0d"]}[Data],
    FACT_WORK_ORDER_SUMMARY_ =
        #"21a564e6-8785-46ed-bcc3-8a78b226cd0d"{[
            entity = "FACT_WORK_ORDER_SUMMARY",
            version = ""
        ]}
            [Data],
    // II.  Specify the desired columns to import from the warehouse
    FACT_WORK_ORDER_SUMMARY_Table =
        Table.SelectColumns(
            FACT_WORK_ORDER_SUMMARY_,
            {
                "SHOP_GL_PROFIT_CENTER_FK",
                "SHOP_GL_COST_CENTER_FK",
                "DIVISION_FK",
                "FIRST_LABOR_DATE",
                "GEOGRAPHY_FK",
                "HEADER_ID",
                "INVOICED_DATE",
                "INVOICED_DATE_FK",
                "INVOICED_INDICATOR",
                "LAST_LABOR_DATE",
                "LAST_LABOR_DATE_FK",
                "OPEN_DATE",
                "OPEN_DATE_FK",
                "SOLD_TO_CUSTOMER_FK",
                "STATUS",
                "TICKET_NUMBER",
                "WORK_ORDER_EQUIPMENT_FK",
                "WORK_ORDER_PK"
            }
        ),
    #"Inserted Date" =
        Table.AddColumn(
            FACT_WORK_ORDER_SUMMARY_Table,
            "OPEN_DATE_DATE",
            each DateTime.Date([OPEN_DATE]),
            type date
        ),
    //  IV.  Specify the data type of each column to maximize accuracy & performance
    #"Changed Type" =
        Table.TransformColumnTypes(
            #"Inserted Date",
            {
                {
                    "WORK_ORDER_PK",
                    Int64.Type
                },
                {
                    "GEOGRAPHY_FK",
                    Int64.Type
                },
                {
                    "DIVISION_FK",
                    Int64.Type
                },
                {
                    "SOLD_TO_CUSTOMER_FK",
                    Int64.Type
                },
                {
                    "WORK_ORDER_EQUIPMENT_FK",
                    Int64.Type
                },
                {
                    "INVOICED_DATE_FK",
                    Int64.Type
                },
                {
                    "SHOP_GL_PROFIT_CENTER_FK",
                    Int64.Type
                },
                {
                    "SHOP_GL_COST_CENTER_FK",
                    Int64.Type
                },
                {
                    "OPEN_DATE_FK",
                    Int64.Type
                },
                {
                    "FIRST_LABOR_DATE",
                    type date
                },
                {
                    "LAST_LABOR_DATE",
                    type date
                },
                {
                    "INVOICED_DATE",
                    type date
                },
                {
                    "HEADER_ID",
                    type text
                },
                {
                    "INVOICED_INDICATOR",
                    type text
                },
                {
                    "STATUS",
                    type text
                },
                {
                    "TICKET_NUMBER",
                    type text
                },
                {
                    "LAST_LABOR_DATE_FK",
                    Int64.Type
                }
            }
        ),
    #"Filtered Rows1" =
        Table.SelectRows(
            #"Changed Type",
            each [OPEN_DATE_DATE] >= EARLIEST_OPEN_DATE
        ),
    #"Filtered Rows" =
        Table.SelectRows(
            #"Filtered Rows1",
            each
                [INVOICED_INDICATOR]
                = "O"
                or [INVOICED_INDICATOR]
                = "I"
                or [INVOICED_INDICATOR]
                = "C"
        ),
    #"Renamed Columns" =
        Table.RenameColumns(
            #"Filtered Rows",
            {
                {
                    "DIVISION_FK",
                    "HDR_DIVISION_FK"
                },
                {
                    "FIRST_LABOR_DATE",
                    "HDR_FIRST_LABOR_DATE"
                },
                {
                    "GEOGRAPHY_FK",
                    "HDR_GEOGRAPHY_FK"
                },
                {
                    "HEADER_ID",
                    "HDR_HEADER_ID"
                },
                {
                    "INVOICED_DATE",
                    "HDR_INVOICED_DATE"
                },
                {
                    "INVOICED_DATE_FK",
                    "HDR_INVOICED_DATE_FK"
                },
                {
                    "INVOICED_INDICATOR",
                    "HDR_INVOICED_INDICATOR"
                },
                {
                    "LAST_LABOR_DATE",
                    "HDR_LAST_LABOR_DATE"
                },
                {
                    "LAST_LABOR_DATE_FK",
                    "HDR_LAST_LABOR_DATE_FK"
                },
                {
                    "OPEN_DATE",
                    "HDR_OPEN_DATE"
                },
                {
                    "OPEN_DATE_FK",
                    "HDR_OPEN_DATE_FK"
                },
                {
                    "SHOP_GL_PROFIT_CENTER_FK",
                    "HDR_SHOP_GL_PROFIT_CENTER_FK"
                },
                {
                    "SHOP_GL_COST_CENTER_FK",
                    "HDR_SHOP_GL_COST_CENTER_FK"
                },
                {
                    "SOLD_TO_CUSTOMER_FK",
                    "HDR_SOLD_TO_CUSTOMER_FK"
                },
                {
                    "STATUS",
                    "HDR_STATUS"
                },
                {
                    "TICKET_NUMBER",
                    "HDR_TICKET_NUMBER"
                },
                {
                    "WORK_ORDER_EQUIPMENT_FK",
                    "HDR_WORK_ORDER_EQUIPMENT_FK"
                },
                {
                    "WORK_ORDER_PK",
                    "HDR_WORK_ORDER_PK"
                },
                {
                    "OPEN_DATE_DATE",
                    "HDR_OPEN_DATE_DATE"
                }
            }
        )
in
    #"Renamed Columns"