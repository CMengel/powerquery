//  This query retreives the changes to GP.
//  This view is unioned/appended with EQUIPMENT_SALES_GP_CHANGE_HISTORY data
//  The combined data is used to compute the Commissions Adjustments measures.
//  This table is loaded into PBI (Enable Load is checked)
let
    //  I. Connection to data source
    Source = PowerPlatform.Dataflows(null),
    Workspaces = Source{[Id = "Workspaces"]}[Data],
    #"009eec1b-1c36-4c96-89f7-c4113ac68c74" = Workspaces{
        [workspaceId = "009eec1b-1c36-4c96-89f7-c4113ac68c74"]
    }[Data],
    #"0f2b6579-a7f8-41ad-880f-6c62741ba6fc" = #"009eec1b-1c36-4c96-89f7-c4113ac68c74"{
        [dataflowId = "0f2b6579-a7f8-41ad-880f-6c62741ba6fc"]
    }[Data],
    FACT_EQUIPMENT_SALES_RPO_CONTRACT_CURR_PAY_PERIOD_GP_CHANGES_ = #"0f2b6579-a7f8-41ad-880f-6c62741ba6fc"{
        [entity = "FACT_EQUIPMENT_SALES_RPO_CONTRACT_CURR_PAY_PERIOD_GP_CHANGES", version = ""]
    }[Data],
    // II. Specify the desired columns to import from the warehouse
    Import_Selected_Cols = Table.SelectColumns(
        FACT_EQUIPMENT_SALES_RPO_CONTRACT_CURR_PAY_PERIOD_GP_CHANGES_,
        {
            "COMMISSION_FREEZE_DATE",
            "CONTRACT_CUSTOMER_FK",
            "CONTRACT_CUSTOMER_ID",
            "CONTRACT_EQUIPMENT_STATUS",
            "CONTRACT_EQUIPMENT_STATUS_DESC",
            "CONTRACT_ID",
            "CURRENT_ADJUSTED_GROSS_PROFIT",
            "CURRENT_TRANSACTION_AMOUNT",
            "DIVISION_FK",
            "EQUIPMENT_ID",
            "FIRST_CLEARING_DATE",
            "GP_CHANGE_AMOUNT",
            "INVOICE_DATE",
            "PREVIOUS_ADJ_GP",
            "PREVIOUS_COMMISSION_LAST_EDIT_PAID_ON",
            //"PREVIOUS_FACT_EQUIPMENT_SALES_RPO_CONTRACT_PK",
            "PREVIOUS_RECORD_CREATED_DATE",
            "PREVIOUS_TRANSACTION_AMOUNT",
            "SALES_REP_FK",
            "SALES_REP_ID"
        }
    ),
    #"Added Custom" = Table.AddColumn(Import_Selected_Cols, "RECORD_DATE", each ""),
    #"Reordered Columns" = Table.ReorderColumns(
        #"Added Custom",
        {
            "COMMISSION_FREEZE_DATE",
            "CONTRACT_CUSTOMER_FK",
            "CONTRACT_CUSTOMER_ID",
            "CONTRACT_EQUIPMENT_STATUS",
            "CONTRACT_EQUIPMENT_STATUS_DESC",
            "CONTRACT_ID",
            "CURRENT_ADJUSTED_GROSS_PROFIT",
            "CURRENT_TRANSACTION_AMOUNT",
            "DIVISION_FK",
            "EQUIPMENT_ID",
            "FIRST_CLEARING_DATE",
            "GP_CHANGE_AMOUNT",
            "INVOICE_DATE",
            "PREVIOUS_ADJ_GP",
            "PREVIOUS_COMMISSION_LAST_EDIT_PAID_ON",
            "PREVIOUS_RECORD_CREATED_DATE",
            "PREVIOUS_TRANSACTION_AMOUNT",
            "RECORD_DATE",
            "SALES_REP_FK",
            "SALES_REP_ID"
        }
    ),
    //  Append the snapshot data to this view
    #"Appended Query" = Table.Combine({#"Reordered Columns", EQUIPMENT_SALES_GP_CHANGE_HISTORY}),
    #"Changed Type" = Table.TransformColumnTypes(#"Appended Query", {{"RECORD_DATE", type date}}),
    //  Add calc col to compute the Sales Comm Pay date and use this column to join to Dim Date
    //  so that adjustment measures appear in the correct time period
    Add_Sales_Comm_Date = Table.AddColumn(
        #"Changed Type",
        "SALES_COMM_PAID_DATE",
        each fxCommissionPaidDate([PREVIOUS_COMMISSION_LAST_EDIT_PAID_ON]),
        type text
    )
in
    Add_Sales_Comm_Date