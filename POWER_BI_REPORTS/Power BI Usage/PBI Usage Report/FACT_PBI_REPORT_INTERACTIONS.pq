//  This query combines data from PBI_ACTIVITY_EVENTS and PBI_DATA_COMBINED
//  The appended data becomes the fact table for the PBI Usage metrics
//  This query is also the source query for all the dimension tables in the model
//  This query *IS* loaded into the semantic model
let
    Source = PBI_ACTIVITY_EVENTS,
    //  combine the tables
    #"Appended Query" = Table.Combine({Source, PBI_DATA_COMBINED}),
    //  remove uneccessary columns
    #"Removed Columns" = Table.RemoveColumns(#"Appended Query", {"CREATIONTIME", "OPERATION"}),
    //  rename columns
    #"Renamed Columns" = Table.RenameColumns(
        #"Removed Columns", {{"CREATIONTIME - Sort", "INTERACTION_DATE"}, {"ACTIVITY", "INTERACTION"}}
    ),
    //  replace nulls with empty string to aid with lookup function in the subsequent step
    Replace_null = Table.ReplaceValue(#"Renamed Columns", null, "", Replacer.ReplaceValue, {"REPORTNAME"}),
    //  create the report name to display in the report
    Add_Report_Name_Display = Table.AddColumn(
        Replace_null,
        "REPORT_NAME_DISPLAY",
        each
            let
            //  determine if the report name is what we're looking for
                ReportNameList = List.Transform(
                    LKP_APP_REPORT_NAMES, (substring) => Text.Contains([REPORTNAME], substring)
                ),
            //  the list of reports we are interested in
                ReportNameReturned = LKP_APP_REPORT_NAMES,
            //  if the report is not found retrun 0 which gives the first item in the list ("Other")
                PositionCheck = List.PositionOf(ReportNameList, true) 
            in
            //  if the report is not found (returns '-1') then return "Other"
                if PositionCheck = -1 then "Other"
                else
                ReportNameReturned{PositionCheck},
                type text
    )
in
    Add_Report_Name_Display