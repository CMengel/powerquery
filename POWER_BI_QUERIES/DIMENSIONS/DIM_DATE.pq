//=== Header Block ===
//  Purpose: support Technician Productivity for Construction
//  Query: DIM_DATE
//  Initial Build Date: 5-6-2021
//  Parameters:
//      CURRENT_WAREHOUSE
//      CURRENT_ROLE
//      CURRENT_DATABASE
//      CURRENT_SCHEMA
//  Original Author: Chris Mengel
// 
let
    //  I. Connection to data source
    Source =
        Snowflake.Databases(
            CURRENT_SERVER,
            CURRENT_WAREHOUSE,
            [
                Role = CURRENT_ROLE,
                CreateNavigationProperties = null,
                ConnectionTimeout = null,
                CommandTimeout = null
            ]
        ),
    CTE_PRE_PROD_DB_Database =
        Source{[
            Name = CURRENT_DATABASE,
            Kind = "Database"
        ]}
            [Data],
    CTE_DW_SCHEMA_Schema =
        CTE_PRE_PROD_DB_Database{[
            Name = CURRENT_SCHEMA,
            Kind = "Schema"
        ]}
            [Data],
    // II. Specify the desired columns to import from the warehouse
    DIM_DATE_Table =
        Table.SelectColumns(
            CTE_DW_SCHEMA_Schema{[
                Name = "DIM_DATE",
                Kind = "Table"
            ]}
                [Data],
            {
                "DATE_PK",
                "DATE_KEY",
                "DAY_OF_WEEK",
                "DAY_OF_WEEK_NUM",
                "DAY_OF_MONTH_NUM",
                "DAY_OF_YEAR_NUM",
                "MONTH_NUM",
                "MONTH_NAME",
                "YEAR_NUM",
                "MONTH_YEAR_NUM",
                "WEEK_OF_YEAR_NUM",
                "WEEK_OF_MONTH_NUM",
                "QUARTER_OF_YEAR_NUM",
                "QUARTER_NAME",
                "ISWEEKEND",
                "US_HOLIDAY",
                "LAST_DAY_OF_MONTH"
            }
        ),
    // III. Filter, Remove Rows, Replace Values
    #"Filtered Rows" =
        Table.SelectRows(
            DIM_DATE_Table,
            each
                [DATE_KEY]
                >= #date(2020, 1, 1)
                and [DATE_KEY]
                <= CurrentYear  //  Date.From(Date.EndOfYear(DateTime.LocalNow()))
        ),
    Add_Month_Year_Column =
        Table.AddColumn(
            #"Filtered Rows",
            "MONTH_YEAR",
            each
                Text.Combine(
                    {
                        [MONTH_NAME],
                        Text.From([YEAR_NUM], "en-US")
                    },
                    "-"
                ),
            type text
        ),
    Add_IsWorkday_Column =
        Table.AddColumn(
            Add_Month_Year_Column,
            "IS_WORKDAY",
            each
                if
                    [ISWEEKEND] = "N"
                    and
                    [US_HOLIDAY] = "N"
                then
                    1
                else
                    0,
            Int64.Type
        ),
    // IV. Selct the data types for each column according to best practices
    //  Maximize performance, efficiency and accuracy of the data model
    #"Changed Type" =
        Table.TransformColumnTypes(
            Add_IsWorkday_Column,
            {
                {
                    "DATE_PK",
                    Int64.Type
                },
                {
                    "DATE_KEY",
                    type date
                },
                {
                    "DAY_OF_WEEK",
                    type text
                },
                {
                    "DAY_OF_WEEK_NUM",
                    Int64.Type
                },
                {
                    "DAY_OF_MONTH_NUM",
                    Int64.Type
                },
                {
                    "DAY_OF_YEAR_NUM",
                    Int64.Type
                },
                {
                    "MONTH_NUM",
                    Int64.Type
                },
                {
                    "YEAR_NUM",
                    Int64.Type
                },
                {
                    "MONTH_YEAR_NUM",
                    Int64.Type
                },
                {
                    "WEEK_OF_YEAR_NUM",
                    Int64.Type
                },
                {
                    "WEEK_OF_MONTH_NUM",
                    Int64.Type
                },
                {
                    "QUARTER_OF_YEAR_NUM",
                    Int64.Type
                },
                {
                    "QUARTER_NAME",
                    type text
                },
                {
                    "ISWEEKEND",
                    type text
                },
                {
                    "US_HOLIDAY",
                    type text
                }
            }
        )
in
    #"Changed Type"